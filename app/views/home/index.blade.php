<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css"></link>

  <link rel="stylesheet" href="css/font-awesome.min.css"></link>

  <link href="css/templatemo_misc.css" rel="stylesheet"></link>

  <link rel="stylesheet" href="css/templatemo_style.css"></link>

  <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:400,100,600"></link>

  <script src="js/jquery-1.10.2.min.js"></script>

  <script src="js/jquery.lightbox.js"></script>

  <script src="js/templatemo_custom.js"></script>

  <script type="text/javascript">

    function showhide()
    {
        var div = document.getElementById("newpost");
        if (div.style.display !== "none")
        {
          div.style.display = "none";
        }
        else {
          div.style.display = "block";
        }
    }
    
  </script>
  <title></title>
</head>
<body>

  <div class="site-header">
    <div class="main-navigation">
      <div class="responsive_menu">
        <ul>          
          <li><a class="show-5 templatemo_page5" href="#">Contact</a></li>
        </ul>
      </div>
      <div class="container">
        <div class="row templatemo_gallerygap">
          <div class="col-md-12 responsive-menu">
            <a href="#" class="menu-toggle-btn">
                    <i class="fa fa-bars"></i>
                </a>
          </div> <!-- /.col-md-12 -->
                    <div class="col-md-3 col-sm-12">
                      <a href="#"><img src="images/templatemo_logo.jpg" alt="Polygon HTML5 Template"></a>
                    </div>
          <div class="col-md-9 main_menu">
            <ul>         
              
              <li><a class="show-5 templatemo_page5" href="#">
                              <span class="fa fa-envelope"></span>
                                Contact</a></li>
            </ul>
          </div> <!-- /.col-md-12 -->

        </div> <!-- /.row -->
      </div> <!-- /.container -->
    </div> <!-- /.main-navigation -->
  </div> <!-- /.site-header -->




    <div id="menu-container">
    <!-- gallery start -->
    <div class="content homepage" id="menu-1">
    <div class="container">
      <div class="row templatemorow">    

            <div class="hex col-sm-6">
              <div>
                  <div class="hexagon hexagon2 gallery-item">
                    <div class="hexagon-in1">
                      <div class="hexagon-in2" style="background-image: url(images/gallery/2.jpg);">
                        <div class="overlay">
                    <a href="images/gallery/2.jpg" data-rel="lightbox" class="fa fa-expand"></a>
                  </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="hex col-sm-6  templatemo-hex-top2">
              <div>
                  <div class="hexagon hexagon2 gallery-item">
                    <div class="hexagon-in1">
                      <div class="hexagon-in2" style="background-image: url(images/gallery/3.jpg);">
                        <div class="overlay">
                    <a href="images/gallery/3.jpg" data-rel="lightbox" class="fa fa-expand"></a>
                  </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="hex col-sm-6  templatemo-hex-top3">
              <div>
                  <div class="hexagon hexagon2 gallery-item">
                    <div class="hexagon-in1">
                      <div class="hexagon-in2" style="background-image: url(images/gallery/4.jpg);">
                        <div class="overlay">
                    <a href="images/gallery/4.jpg" data-rel="lightbox" class="fa fa-expand"></a>
                  </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

   
      </div>
  </div>
    <!-- <div class="container">
      <div class="row">
          <div class="templatemo_loadmore">
       <button class="gallery_more" id="button" onClick="showhide()">Load More</button>
            </div>
        </div>
    </div> -->
    </div>
      <!-- gallery end -->
   

      <br><br><br><br><br><br>
    <div class="clear"></div>


    <!-- contact start -->
    <div class="content contact" id="menu-5">
    <div class="container">
      <div class="row">
          <div class="col-md-4 col-sm-12">
              <div class="templatemo_contactmap">
          <div id="templatemo_map"></div>
                <img src="images/templatemo_contactiframe.png" alt="contact map">
                </div>
                </div>
            <div class="col-md-3 col-sm-12 leftalign">
              <div class="templatemo_contacttitle">Contact Information</div>
                <div class="clear"></div>
                <p>Integer eu neque sed mi fringilla pellentesque a eget leo. Duis ornare diam lorem, sit amet tempor mauris fringilla in. Etiam semper tempus augue, at vehicula metus. Nam vestibulum tortor nec congue ornare.</p>
                <div class="templatemo_address">
                  <ul>
                  <li class="left fa fa-map-marker"></li>
                    <li class="right">Nulla ut tellus, sit amet urna, <br>scelerisque pretium 10560</li>
                    <li class="clear"></li>
                    <li class="left fa fa-phone"></li>
                    <li class="right">010-040-0260</li>
                    <li class="clear"></li>
                    <li class="left fa fa-envelope"></li>
                    <li class="right">info@company.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
              <form role="form">
                <div class="templatemo_form">
                  <input name="fullname" type="text" class="form-control" id="fullname" placeholder="Your Name" maxlength="40">
                </div>
                <div class="templatemo_form">
                  <input name="email" type="text" class="form-control" id="email" placeholder="Your Email" maxlength="40">
                </div>
                <div class="templatemo_form">
                  <input name="subject" type="text" class="form-control" id="subject" placeholder="Subject" maxlength="40">
                </div>
                <div class="templatemo_form">
                <textarea name="message" rows="10" class="form-control" id="message" placeholder="Message"></textarea>
                </div>
                <div class="templatemo_form"><button type="button" class="btn btn-primary">Send Message</button></div>
            </form>
            </div>
        </div>
      
    </div>
    </div> 

    </div>
    <!-- contact end -->



  <!-- footer start -->
    <div class="templatemo_footer">
      <div class="container">
      <div class="row">
          <div class="col-md-9 col-sm-12">Copyright &copy; 2084 Company Name | 
            Photos by <a rel="nofollow" href="http://unsplash.com">Unsplash</a></div>
            <div class="col-md-3 col-sm-12 templatemo_rfooter">
                <a href="#">
                  <div class="hex_footer">
          <span class="fa fa-facebook"></span>
          </div>
                  </a>
                  <a href="#">
                    <div class="hex_footer">
           <span class="fa fa-twitter"></span>
          </div>
                    </a>
                  <a href="#">
                  <div class="hex_footer">
           <span class="fa fa-linkedin"></span>
          </div>
                 </a>
                <a href="#">
                  <div class="hex_footer">
           <span class="fa fa-rss"></span>
          </div>
                </a>
            </div>
        </div>
        </div>
    </div>
    <!-- footer end -->    
  <script>
  $('.gallery_more').click(function(){
    var $this = $(this);
    $this.toggleClass('gallery_more');
    if($this.hasClass('gallery_more')){
      $this.text('Load More');      
    } else {
      $this.text('Load Less');
    }
  });
    </script>


</body>
</html>